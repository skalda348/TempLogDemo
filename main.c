/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body of demo application for log temperature
  *                   to EEPROM
  ******************************************************************************
  * @attention
  *
  * This software component is licensed under GNU-GPL
  *
  ******************************************************************************
  */
 
/* Includes ------------------------------------------------------------------*/
#include "LOG_driver.h"

int main(void)
{
    uint16_t temp;
    uint32_t time;
    uint8_t oBits;
    LogValue to_log;

    // initialize peripherals
    ALL_Init();
    // initialize LOG memory driver
    LOG_MemInit();

    //infinite loop
    while (1)
    {   
        // get act time 
        time = GetTimeMinutes();
        // read I2C temperature
        temp = I2C_readTemp();
        // get option Bits
        oBits = ManageOptions();
        // store data to eeprom
        to_log.temp = temp;
        to_log.statBits = oBits;
        to_log.time_stamp = time;
        if (LOG_StoreTemp(to_log) != LOG_RET_OK) ErrorManage(LOG_STORE_ERROR);
        // sleep until absolute time happend
        SystemSleepUntilMinutes(time + TEMP_STORE_INTERVAL_MINUTES);
    }

}