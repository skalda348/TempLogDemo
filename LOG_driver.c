/**
  ******************************************************************************
  * @file           : LOG_driver.c
  * @brief          : Library managing store temperature log into EEPROM memory
  ******************************************************************************
  * @attention
  * 
  * This software component is licensed under GNU-GPL
  *
  ******************************************************************************
  */



/* Includes ------------------------------------------------------------------*/
#include "LOG_driver.h"
#include "EEPROM_driver.h"

/* Private function prototypes -----------------------------------------------*/
uint8_t LOG_WriteTimeStamp(uint32_t addr, uint32_t timeStamp);
uint8_t LOG_WriteTempData(uint32_t addr, LogValue data);
uint8_t LOG_ParseTempData(uint32_t addr, LogValue data);
uint8_t LOG_isTimeStampValid(uint32_t timeStamp);
uint8_t LOG_FreeMemory(uint32_t * startAddr, uint8_t * isTimeStampNeeded);

/**
  * @brief  Function managing Store of data to EEPROM
  * @param  toStore: data structure with data to store to EEPROM
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_StoreTemp(LogValue toStore)
{
    uint8_t to_ret = LOG_RET_OK;
    uint8_t isTimeStampNeeded = FALSE;
    uint32_t timeStamp, timeStampOffset, addrToWrite;
    uint32_t size;
    if (EEPROM_getSize(&size) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (LOG_FindMemSpace(&addrToWrite, &timeStamp, &timeStampOffset)) to_ret = LOG_RET_NOT_OK;
    // check if time stamp write is needed
    if (!LOG_isTimeStampWalid(timeStamp) || !LOG_IsTimeStampDiffInLimit(timeStamp, timeStampOffset, toStore))
    {
        isTimeStampNeeded = TRUE;
    }
    if (timeStampOffset > MAT_TIME_STAMPS_OFFSET)
    {        
        isTimeStampNeeded = TRUE;
    }
    
    // check if is enought free memory to write 
    if (LOG_FreeMemory(&addrToWrite, &isTimeStampNeeded) == LOG_RET_OK)
    {
        if (isTimeStampNeeded == TRUE)
        {
            if (LOG_WriteTimeStamp(addrToWrite, toStore.time_stamp) != LOG_RET_OK) to_ret = LOG_RET_NOT_OK;
            addrToWrite += 4;
        }
        if (LOG_WriteTempData(addrToWrite, toStore) != LOG_RET_OK) to_ret = LOG_RET_NOT_OK;

    }
    else
    {
        to_ret = LOG_RET_NOT_OK;
    }
        
    return to_ret;
}

/**
  * @brief  Function for read last stored value
  * @param  None
  * @retval LogValue : data structure with data last time stored into EEPROM
  */
LogValue LOG_LastTemp(void)
{
    LogValue log;
    uint32_t timeStamp, timeStampOffset, addrToWrite;
    uint32_t size;
    LOG_FindMemSpace(&addrToWrite, &timeStamp, &timeStampOffset);
    log.time_stamp = timeStamp;
    LOG_ParseTempData(addrToWrite - 2, &log)
    return log;
}

/**
  * @brief  Function for init memory
  * @param  None
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_MemInit()
{
    uint8_t to_ret = TRUE;
    uint32_t i, size;
    if (EEPROM_getSize(&size) != EEPROM_RET_OK) to_ret = FALSE;
    
    for (i = 0, i < size; i++)
    {
        if (EEPROM_erraseByte(i) != EEPROM_RET_OK) to_ret = FALSE;
    }

    return to_ret;
}

/**
  * @brief  Function for find space in memory which is ready to write
  * @param  freeAddr: return parameter with found addr
  * @param  lastTimeStamp: return value of last time stamp in EEPROM memory
  * @param  tsOffset: offste of freeAddr from lastTimeStamp position in memory
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_FindMemSpace(uint32_t * freeAddr, uint32_t * lastTimeStamp, uint32_t * tsOffset)
{
    uint8_t to_ret = LOG_RET_OK;
    uint8_t data;
    uint32_t timeStamp = 0x00000000;
    uint32_t timeStampOffset = 0;
    uint32_t i, size;
    *lastTimeStamp = timeStamp;
    if (EEPROM_getSize(&size) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    
    for (i = 0, i < size; i+=2)
    {
        if (EEPROM_readByte((i, &data)) == EEPROM_RET_OK)
        {
            // find time stamp
            if (data & TIME_STAMP_BIT_MASK == TIME_STAMP_BIT_MASK && data != 0xFF)
            {
                timeStamp |= data << 24;
                EEPROM_readByte((i+1, &data)) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
                timeStamp |= data << 16;
                EEPROM_readByte((i+2, &data)) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
                timeStamp |= data << 8;
                EEPROM_readByte((i+3, &data)) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
                timeStamp |= data;
                if (timeStamp < *lastTimeStamp)
                { 
                     // posiotion for write is found
                    break; 
                } 
                else
                {
                    *lastTimeStamp = timeStamp;
                    timeStampOffset = 0;
                    i = i + 2;                    
                }
                
            }
            else if (data == 0xFF)
            {
                break;
            }
            else
            {
                timeStampOffset++;
            }
            
        }
        else
        {
            to_ret = LOG_RET_NOT_OK;
        }
    }
    *freeAddr = i;
    *lastTimeStamp = timeStamp;
    *tsOffset = timeStampOffset;
    return to_ret;
}

/**
  * @brief  Function for write time stamp into memory
  * @param  addr: addres where data will be writen
  * @param  timeStamp: data to write
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_WriteTimeStamp(uint32_t addr, uint32_t timeStamp)
{
    uint8_t to_ret = LOG_RET_OK;
    if (EEPROM_writeByte(addr, (((timeStamp >> 24) & 0xFF)  & ~TIME_STAMP_NOT_EMPTY_BIT_MASK) | TIME_STAMP_BIT_MASK) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (EEPROM_writeByte(addr + 1, (timeStamp >> 16) & 0xFF) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (EEPROM_writeByte(addr + 2, (timeStamp >> 8) & 0xFF) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (EEPROM_writeByte(addr + 3, (timeStamp) & 0xFF) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;

    return to_ret;
}

/**
  * @brief  Function for write temperature and option bits to EEPROM
  * @param  addr: addres where data will be writen
  * @param  data:  data to write
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_WriteTempData(uint32_t addr, LogValue data)
{
    uint8_t to_ret = LOG_RET_OK;
    uint8_t dataPrepared = 0x00;
    dataPrepared = (data.temp >> 8) & 0xFF;
    dataPrepared &= ~TEMP_STATE_BITS_MASK;
    dataPrepared |= (data.statBits << TEMP_STATE_BITS_SHIFT) & TEMP_STATE_BITS_MASK;
    dataPrepared &= ~TIME_STAMP_BIT_MASK;
    if (EEPROM_writeByte(addr, dataPrepared) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (EEPROM_writeByte(addr + 1, (data.temp & 0xFF)) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    return to_ret;
}

/**
  * @brief  Function for read and parse stored temperature and option bits from EEPROM
  * @param  addr: addres where to read data
  * @param  data: pointer to output structure for loaded data
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_ParseTempData(uint32_t addr, LogValue * data)
{    
    uint8_t to_ret = LOG_RET_OK;
    uint8_t memData;
    if (EEPROM_readByte(addr, &memData) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    if (memData & TIME_STAMP_BIT_MASK = 0)
    {
        data.statBits = (memData & TEMP_STATE_BITS_MASK) >> TEMP_STATE_BITS_SHIFT;
        data.temp = (memData & TEMP_VALUE_H_MASK) << 8        
        if (EEPROM_readByte(addr, &memData) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;  
        data.temp |= memData;
    }
    else
    {
        to_ret = LOG_RET_NOT_OK;
    }
    

    return to_ret;
}

/**
  * @brief  Function for test time stamp as valid or not
  * @param  timeStamp: time stamp to test
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_isTimeStampValid(uint32_t timeStamp)
{
    uint8_t to_ret = LOG_RET_OK;
    if (time_stamp != 0xFFFFFFFF) to_ret = LOG_RET_NOT_OK;
    if (time_stamp & TIME_STAMP_BIT_MASK != TIME_STAMP_BIT_MASK)  to_ret = LOG_RET_NOT_OK;

    return to_ret;
}

/**
  * @brief  Function for test if data stored in EEPROM last is in right period from actual data. If not new time stamp is necessary to save to EEPROM.
  * @param  timeStamp: time stamp last stored in EEPROM
  * @param  timeStampOffset: offset of last stored data from last stored time stamp
  * @param  toStore: actual time stamp to store
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_IsTimeStampDiffInLimit(uint32_t timeStamp, uint32_t timeStampOffset,LogValue toStore)
{
    uint8_t to_ret = LOG_RET_OK;
    if ((timeStamp + timeStampOffset * TEMP_STORE_INTERVAL_MINUTES) - toStore.time_stamp > MAX_STORE_JITTER_MINUTES) to_ret = LOG_RET_NOT_OK;

    return to_ret;
}

/**
  * @brief  Function for testing if memory is ready to save (not empty and enough space). In case not enough memory
  *         it manage delete page of oldest data in EEPROM.
  * @param  startAddr: pointer to adress where to save. Function modify it when it is necessary
  * @param  isTimeStampNeeded: pointer to BOOL decribe if time stamp is needed to save too (aditional 4bytes)
  * @retval uint8_t: LOG_RET_OK          (0)
  *                  LOG_RET_NOT_OK      (1)
  */
uint8_t LOG_FreeMemory(uint32_t * startAddr, uint8_t * isTimeStampNeeded)
{
    uint8_t to_ret = LOG_RET_OK;
    uint8_t isEmpty = TRUE;
    uint8_t reqSize;
    uint8_t data;
    uint32_t i, size;
    if (EEPROM_getSize(&size) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;

    if (*isTimeStampNeeded == TRUE)
    {
        reqSize = 6;
    }
    else 
    {
        reqSize = 2;
    }
    // check if is not enought memory until end of memory
    if (*startAddr + reqSize >= size)
    {
        *startAddr = 0;
        *isTimeStampNeeded = TRUE;
        reqSize = 6;
    }
    // check if requested memory is empty
    for (i = *startAddr; ((i <= *startAddr+reqSize) && (i < size)); i++)
    {
        if (EEPROM_readByte(i, &data) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
        if (data != 0xFF) 
        {
            isEmpty = FALSE;
            break;
        }
    }

    // manage errase of EEPROM when needed
    if (!isEmpty) 
    {       
        if (EEPROM_errasePage(i) != EEPROM_RET_OK) to_ret = LOG_RET_NOT_OK;
    }
   
    return to_ret;
}
