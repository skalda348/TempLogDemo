/**
  ******************************************************************************
  * @file           : EEPROM_driver.h
  * @brief          : Pseudo interface for EEPROM memory 
  ******************************************************************************
  * @attention
  *
  * This software component is licensed under GNU-GPL
  *
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_DRIVER_H__
#define __EEPROM_DRIVER_H__

/* Exported constants --------------------------------------------------------*/
#define EEPROM_RET_OK       (0)
#define EEPROM_RET_NOT_OK   (1)

/* Exported functions prototypes ---------------------------------------------*/
uint8_t EEPROM_getSize(uint32_t * size);
uint8_t EEPROM_readByte(uint32_t address, uint8_t * data);
uint8_t EEPROM_writeByte(uint32_t address, uint8_t data);
uint8_t EEPROM_errasePage(uint32_t startAddress)


#endif 