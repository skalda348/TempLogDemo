/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines and function 
  *                   protypes for all library.
  ******************************************************************************
  * @attention
  *
  * This software component is licensed under GNU-GPL
  *
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LOG_DRIVER_H__
#define __LOG_DRIVER_H__

/* Includes ------------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
#define TIME_STAMP_BIT_MASK                 (0x80)
#define TIME_STAMP_NOT_EMPTY_BIT_MASK      (0x40)

#define TEMP_STATE_BITS_SHIFT    (3)
#define TEMP_STATE_BITS_MASK    ((0xF)<<TEMP_STATE_BITS_SHIFT)
#define TEMP_VALUE_H_MASK       ((0x7))

#define TEMP_STORE_INTERVAL_MINUTES         (30)
#define MAX_STORE_JITTER_MINUTES            (1)
#define MAT_TIME_STAMPS_OFFSET              (48)

#define LOG_RET_OK          (0)
#define LOG_RET_NOT_OK      (1)

/* Exported types ------------------------------------------------------------*/
typedef struct LogValue
{
    int16_t temp;
    uint8_t statBits;
    uint32_t time_stamp;
};

typedef struct pointerValue
{
    uint16_t timeStampAddrOffset;
    uint16_t actValOffset;

};

/* Exported functions prototypes ---------------------------------------------*/
uint8_t LOG_StoreTemp(LogValue toStore);
LogValue LOG_LastTemp(void);
uint8_t LOG_MemInit();
uint8_t LOG_FindMemSpace(uint32_t * freeAddr, uint32_t * lastTimeStamp, uint32_t * tsOffset);

#endif