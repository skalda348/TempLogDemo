# TempLogDemo

Demo of library to store data from temperature sensor into EEPROM memory. EEPROM memory is used as cyclic buffer so in case of full memory are oldest data deleted.

# Memory Organization

For save memory data are splited into time stamp folowed by measured temperatures and option bits array. All values in array are stored in exact time interval defined by TEMP_STORE_INTERVAL_MINUTES in LOG_driver.h.
In case of no exact interval new time stamp is sored into EEPROM.

All data in EEPROM are aligned to 2Byte. Time stamp consume 4Bytes and temperature data 2Bytes. Difference betveen time stamp and data can be identified by MSB bit byte stored in lowest address. It is set to 1 for time stamp and to zero for data. For identify valid time stamp from empty memory bit MSB>>1 is 0 for valid time stamp.

# Data limitations

**Time stamp**
Time stamp is stored as 30bit number and explain absolute epoch in minutes from time defined by master system of this library. It can store 0x3FFF FFFF minutes. It is approx. 17 895 697 hours. It is 745654 days. It is approx 2042 years. Is is to much but lover bit values is not convenient becouse of lost of 2Byte alignment. There is space where could be implemented checksums or other control mechanisms in case or reduce max. store value.

**Temperature**
Temperature is stored as 11bit signed number and represent values in precisness 0,5 degree of Celsius. Supported values inteval is from -511°C to 512,5°C.

**Store capacity requirements**
Memory capacity requirements depends frequency of store time stamps and frequeny of log data and other settings. 
For example when log temperature each 30min ti nead approx.  100Bytes per day. In case of 4kBytes memory, you can save data from 40 days until new data override old one.

